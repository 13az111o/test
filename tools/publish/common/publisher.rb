require 'net/http'
require 'json'
require 'erb'
require 'uri'
module ReleaseTools
  class WikiPublisher

    def initialize(args)
      $token = args[0]
      $api_url = args[1]
      $project_id = args[2]
      $commit_sha = args[3]
      $pipeline_iid = args[4]

      # Получаем json с mr-ками коммита
      json = get_mrs

      if parse_json(json)

        template = File.read("common/template/template.erb")
        renderer = ERB.new(template)

        # заголовок документа
        $md_title = "SFW #{$pipeline_iid} sha: #{@mr_sha.match(/^[0-9a-z]{8}/)} mr: #{@mr_number}"

        # содержание документа
        $md_content = renderer.result(binding)

        # отправляем документ
        create_wiki_doc
      end
    end

# Парсим JSON
    def parse_json(json)
      puts "Parsing json..."
      if json.nil? || !json.match(/^\{\"message\":\"4*/).nil?
        puts "Error..."
        puts json
        return false
      else
        puts "Parsed successfully.."
        parsed = JSON.parse(json)
      end
      if parsed != nil && parsed.size > 0
        parsed.each do |element|
          @mr_number = element["iid"]
          @mr_title = element["title"]
          @mr_description = element["description"]
          @mr_sha = element["merge_commit_sha"]
          puts "MR was found.."
          return true
        end
      else
        puts "No MR`s were found.."
        false
      end
    end


# получение MRs коммита если есть
    def get_mrs()
      puts "Getting mr`s.."
      url = "#{$api_url}/projects/#{$project_id}/repository/commits/#{$commit_sha}/merge_requests?private_token=#{$token}"
      uri = URI.parse(url)
      res = Net::HTTP.get_response(uri)
      "#{res.body}"
    rescue => e
      puts "failed #{e}"
    end


# отправка документа на wiki gitlab
    def create_wiki_doc()
      puts "Creating wiki docs.."
      url = "#{$api_url}/projects/#{$project_id}/wikis"
      uri = URI.parse(url)

      params = {
          #  :format => "rdoc",
          :title => "#{$md_title}",
          :content => "#{$md_content}"
      }.to_json

      headers = {"PRIVATE-TOKEN" => "#{$token}", "Content-Type" => "application/json", }

      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      res = http.post(uri.path, params, headers)
    end
  end
end
