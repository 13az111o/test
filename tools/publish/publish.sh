#!/bin/bash
# Обновляет гитлаб wiki проекта
#
# Пример
#  ./tools/publish/publish.sh --gitlab-token '<your_access_token>' --api-url 'https://gitlab.example.com/api/v4'  --project-id '100' --commit-sha '7a247d771de3114790a038b9d4ef8b34ec2b2dca' --pipeline-iid '123'"

 POSITIONAL=()
 while [[ $# -gt 0 ]]; do
   key="$1"

   case $key in
     --gitlab-token)
       GITLAB_TOKEN="$2"
       shift # past argument
       shift # past value
       ;;
     --api-url)
       API_URL="$2"
       shift # past argument
       shift # past value
       ;;
     --project-id)
       PROJECT_ID="$2"
       shift # past argument
       shift # past value
       ;;
     --commit-sha)
       COMMIT_SHA="$2"
       shift # past argument
       shift # past value
       ;;
     --pipeline-iid)
       CI_PIPELINE_IID="$2"
       shift # past argument
       shift # past value
       ;;
     *)    # unknown option
       POSITIONAL+=("$1") # save it in an array for later
       shift # past argument
       ;;
   esac
 done

set -- "${POSITIONAL[@]}" # restore positional parameters

if [[( -z "$GITLAB_TOKEN" ) || ( -z "$API_URL" ) || ( -z "$PROJECT_ID" ) || ( -z "$COMMIT_SHA" ) || ( -z "$CI_PIPELINE_IID" )]] ; then
    echo "One or more arguments are missing!"
    echo "Example:  ./tools/publish/publish.sh --gitlab-token '<your_access_token>' --api-url 'https://gitlab.example.com/api/v4' --project-id '100' --commit-sha '7a247d371de3114790a038b9d4ef8b34ec2b2dca' --pipeline-iid '123'"
    exit 1
else
    echo "gitlab_token is present"
    echo "api_url = ${API_URL}"
    echo "project_id = ${PROJECT_ID}"
    echo "commit_sha = ${COMMIT_SHA}"
    echo "pipline_iid = $CI_PIPELINE_IID"
fi

DIR=$(dirname "$0")

echo "tools-dir : $DIR"

# cd "$DIR" || { echo "Directory ${DIR} not found!"; exit 1; }

ruby common/release-tools.rb $GITLAB_TOKEN $API_URL $PROJECT_ID $COMMIT_SHA $CI_PIPELINE_IID
